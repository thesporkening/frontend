## Frontend
A [Next.js](http://nextjs.org/) application to display data from the backend

## Tech stack
| TailwindCSS
| Next.JS
| React
| Node |

## How to run
Open console and run 

`npm install`

`npm run dev`