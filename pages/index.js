import Layout from '../components/layout'
import withPage from '../providers/page'

export default withPage(() => {
  return <Layout>
      <div className="p-4 shadow rounded bg-white mt-4">
          <h1 className="text-purple leading-normal">Next.js</h1>
          <p className="text-grey-dark">with Tailwind CSS</p>
      </div>
    </Layout>  
})
