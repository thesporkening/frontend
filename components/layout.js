import Head from 'next/head'
import Link from 'next/link'
export default ({ children }) => {
  /*
   * Added this to toggle the is-active class. See:
   *
   * https://bulma.io/documentation/components/navbar/#navbar-menu
   * https://github.com/jgthms/bulma/issues/856
   */
  const toggleStyles = (event) => {
    document.querySelector('#burger').classList.toggle('is-active');
    document.querySelector('#navbarmenu').classList.toggle('is-active');
  };

  return (
      <div>
        <Head>
          <title>TheSystemProject</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
          <link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet"/>
        </Head>
        <header>
          <nav className="flex items-center justify-between flex-wrap bg-teal-500 p-6">
            <div className="flex items-center flex-shrink-0 text-white mr-6">
              <span className="font-semibold text-xl tracking-tight">The System Project</span>
            </div>
            <div className="block lg:hidden">
              <button
                  className="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
              </button>
            </div>
            <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
              <div className="text-sm lg:flex-grow">
                <Link prefetch href="/">
                  <a className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4">Home</a>
                </Link>
                <Link prefetch href="/features">
                  <a className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4">Features</a>
                </Link>
                <Link prefetch href="/about">
                  <a className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white">About</a>
                </Link>
              </div>
              <div>
                <a href="#"
                   className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0">Download</a>
              </div>
            </div>
          </nav>
        </header>
        <div className="container mx-auto">
          {children}
        </div>
{/*        <footer className="footer">
          <div className="container mx-auto">
            <div>
              <div>
                <h2><strong>Category</strong></h2>
                <ul>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Vestibulum errato isse</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Aisia caisia</a></li>
                  <li><a href="#">Murphy's law</a></li>
                  <li><a href="#">Flimsy Lavenrock</a></li>
                  <li><a href="#">Maven Mousie Lavender</a></li>
                </ul>
              </div>
              <div>
                <h2><strong>Category</strong></h2>
                <ul>
                  <li><a href="#">Labore et dolore magna aliqua</a></li>
                  <li><a href="#">Kanban airis sum eschelor</a></li>
                  <li><a href="#">Modular modern free</a></li>
                  <li><a href="#">The king of clubs</a></li>
                  <li><a href="#">The Discovery Dissipation</a></li>
                  <li><a href="#">Course Correction</a></li>
                  <li><a href="#">Better Angels</a></li>
                </ul>
              </div>
              <div>
                <h2><strong>Category</strong></h2>
                <ul>
                  <li><a href="#">Objects in space</a></li>
                  <li><a href="#">Playing cards with coyote</a></li>
                  <li><a href="#">Goodbye Yellow Brick Road</a></li>
                  <li><a href="#">The Garden of Forking Paths</a></li>
                  <li><a href="#">Future Shock</a></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>*/}
      </div>
  )
}